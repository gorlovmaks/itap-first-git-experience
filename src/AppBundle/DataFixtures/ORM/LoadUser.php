<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use AppBundle\Entity\UserImages;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



class LoadUser implements FixtureInterface, ORMFixtureInterface
{



    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    // Фикстура корявая . Без фикстур всё работает)
    public function load(ObjectManager $manager)
    {

        for($i = 0; $i < 7; $i++){
            $user = new User();

            $password = $this->encoder->encodePassword($user, '123');
            $user
                ->setEmail('user'.$i.'@mail.com')
                ->setPassword($password)
                ->setUsername('Gavrilko'.$i.'')
                ->setRoles(array('ROLE_USER'))
                ->setEnabled(true)
                ->addFriend($user);

            $manager->persist($user);

            $post = new UserImages();
                $post
                    ->setTitle('Бла бла бла')
                    ->setUser($user)
                    ->setPicture('5b0c301c3e014.jpg')
                    ->addLiker($user);
            $manager->persist($post);
        }
        $manager->flush();

    }
}
