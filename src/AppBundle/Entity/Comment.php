<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=127)
     */
    private $comment;


    /**
     * @var UserImages
     *
     * @ORM\ManyToOne(targetEntity="UserImages", inversedBy="Comments")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     *
     *
     */
    private $UserImage;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="Comments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     *
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }


    /**
     * @return UserImages
     */
    public function getUserImage(): UserImages
    {
        return $this->UserImage;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param UserImages $UserImage
     */
    public function setUserImage(UserImages $UserImage)
    {
        $this->UserImage = $UserImage;
    }


}

