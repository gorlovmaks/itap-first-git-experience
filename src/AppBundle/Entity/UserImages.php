<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;


/**
 * UserImages
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserImagesRepository")
 * @Vich\Uploadable
 */
class UserImages
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     *
     * */
    private $title;

    /**
     * @var string
     *
     *@ORM\Column(name="picture", type="string", nullable=true)
     */
    private $picture;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="picture_file", fileNameProperty="picture")
     *
     * @var File
     */
    private $imageFile;


    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="images")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     *
     */
    private $user;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="UserImage")
     */

    private $comments;

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", indexBy="id", cascade={"persist"})
     */
    public $likers;

    public function __construct()
    {
        $this->likers = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @param  User $liker
     */
    public function addLiker(User $liker)
    {
        $this->likers->add($liker);
    }

    public function getCountLikers()
    {
        return $this->likers->count();
    }

    public function contains(User $liker) {
        return $this->likers->contains($liker);
    }

    public function removeLikes(User $liker){
        return $this->likers->removeElement($liker);
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param User $user
     * @return UserImages
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $picture
     * @return UserImages
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }



    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikers()
    {
        return $this->likers;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function addComment(Comment $comment){
        $this->comments->add($comment);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     *
     * @return UserImages
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

}