<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserImages", mappedBy="user")
     */

    private $images;


    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User")
     */
    private $friends;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment", mappedBy="User")
     */

    private $comments;

    public function __construct()
    {
        parent::__construct();
        $this->images = new ArrayCollection();
        $this->friends = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }


    /**
     * @return ArrayCollection
     */
    public function getImage()
    {
        return $this->images;
    }


    /**
     * @param  UserImages $image
     */

    public function addImage(UserImages $image){
        $this->images->add($image);
    }


    /**
     * @param  User $user
     */
    public function addFriend(User $user){
        $this->friends->add($user);
    }


    /**
     * @return ArrayCollection
     */
    public function getFriends(): ArrayCollection
    {
        return $this->friends;
    }

    public function hasFollower(User $user){
        return $this->friends->contains($user);
    }

    public function removeFollower(User $user){
        $this->friends->removeElement($user);
    }

    /**
     * @return ArrayCollection
     */
    public function getComments(): ArrayCollection
    {
        return $this->comments;
    }

}
