<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Comment;
use AppBundle\Entity\UserImages;
use AppBundle\Form\CommetForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ImagesController extends Controller
{
    /**
     * @Route("/images")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ImageListAction(Request $request)
    {
        $user = $this->getUser();
        $images = $this->getDoctrine()
            ->getRepository('AppBundle:UserImages')
            ->findAll();


        return $this->render('@App/ImagesControlles/tape.html.twig', array(
            'images' => $images,
            'user' => $user,
        ));
    }

     /**
      * @Route("/images/likes/{id}", requirements={"id": "\d+"})
      * @Method({"GET","HEAD"})
      * @param $id integer
      * @return \Symfony\Component\HttpFoundation\Response
      *
      */
    public function addLikesAction(int $id){

        $user = $this->getUser();

        $image = $this->getDoctrine()
            ->getRepository('AppBundle:UserImages')
            ->find($id);

        $image->addLiker($user);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute("app_images_imagelist");

    }

  /**
      * @Route("/images/removelikes/{id}", requirements={"id": "\d+"})
      * @Method({"GET","HEAD"})
      * @param $id integer
      * @return \Symfony\Component\HttpFoundation\Response
      *
      */
    public function removeLikes(int $id){

        $user = $this->getUser();

        $image = $this->getDoctrine()
            ->getRepository('AppBundle:UserImages')
            ->find($id);

        $image->removeLikes($user);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute("app_images_imagelist");
    }

    /**
     * @Route("/images/comments/{id}", requirements={"id": "\d+"})
     * @param Request $request
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function commentList(Request $request, int $id){
        $user = $this->getUser();
        $comment = new Comment();
        $image = $this->getDoctrine()
            ->getRepository('AppBundle:UserImages')
            ->find($id);

        $form = $this->createForm(
            CommetForm::class,
            $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setUser($user);
            $comment->setUserImage($image);

            $image->addComment($comment);

            $em = $this->getDoctrine()->getManager();

            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute('app_images_commentlist', array('id' => $image->getId()));
        }
        $comments = $image->getComments();


        return $this->render('@App/ImagesControlles/commentList.html.twig', array(
            'comments' => $comments,
            'form' => $form->createView()
        ));
    }
}
