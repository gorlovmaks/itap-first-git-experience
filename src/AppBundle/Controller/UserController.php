<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserImages;
use AppBundle\Form\ImageForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{

    /**
     * @Route("/person")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function personalArea(Request $request)
    {
        $user = $this->getUser();

        $image = new UserImages();
        $form = $this->createForm(
            ImageForm::class,
            $image);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $image->setUser($user);
            $em = $this->getDoctrine()->getManager();

            $em->persist($image);
            $em->flush();

            return $this->redirectToRoute('app_user_personalarea', array('id' => $user->getId()));
        }
        $post = $user->getImage();
        return $this->render('@App/User/person.html.twig', array(
            'posts' => $post,
            'form' => $form->createView(),
            'user' => $user
        ));
    }


    /**
     * @Route("/images/followers/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function addFollowers(int $id)
    {
        $user = $this->getUser();
        $seller_user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);

        $user->addFriend($seller_user);
        $seller_user->addFriend($user);


        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute("app_images_imagelist");
    }


    /**
     * @Route("/images/removefollower/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function removeFollowers(int $id){
        $user = $this->getUser();

        $seller_user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);
        $user->removeFollower($seller_user);
        $seller_user->removeFollower($user);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute('app_images_imagelist');
    }
}
